<?php

namespace Pukudada\Loyalty;

use Illuminate\Database\Eloquent\Model;

class LoyaltySetting extends Model
{
    protected $table = 'loyalty_setting';

    protected $guarded = [];
}
