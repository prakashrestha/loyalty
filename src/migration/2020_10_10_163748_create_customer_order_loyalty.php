<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Pukudada\Loyalty\LoyaltySetting;

class CreateCustomerOrderLoyalty extends Migration
{

    public function up()
    {
        DB::beginTransaction();

//        $loyalty_table = config('loyalty_config.loyalty_table');
//        $user_table = config('loyalty_config.users_table', 'ro_users');
//        $order_table = config('loyalty_config.order_table', 'ro_orders');
//        $userforeign = config('loyalty_config.user_foreign_key');
//        $orderforeign = config('loyalty_config.order_foreign_key');


        Schema::create('customer_order_loyalty', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('order_id')->nullable();
            $table->boolean('manual_loyalty')->default(0);
            $table->float('loyalty_points')->nullable();
            $table->float('redeem_points')->nullable();
            $table->float('redeem_value')->nullable();


            $table->foreign('user_id')->references('id')->on(config('loyalty_config.users_table', 'ro_users'))->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('order_id')->references('id')->on(config('loyalty_config.order_table', 'ro_orders'))->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('customer_loyalty', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->float('loyalty_total');
            $table->float('redeem_point')->nullable();
            $table->dateTime('redeem_at')->nullable();
            $table->float('loyalty_remain')->default(0);
            $table->foreign('user_id')->references('id')->on(config('loyalty_config.users_table', 'ro_users'))->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });



        Schema::create('loyalty_setting' , function (Blueprint $table) {

            $table->increments('id');
            $table->float('expense_amt')->nullable();
            $table->float('loyalty_conversion')->nullable();
            $table->float('points')->nullable();
            $table->float('monetary_conversion')->nullable();
            $table->float('redeem_threshold')->nullable();
            $table->timestamps();

        });

        LoyaltySetting::create(['expense_amt' => 10, 'loyalty_conversion' => 1,'points' => 10, 'monetary_conversion' => 1, 'redeem_threshold'=> 1000]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_order_loyalty');
        Schema::dropIfExists('customer_loyalty');
        Schema::dropIfExists('loyalty_setting');
    }
}
