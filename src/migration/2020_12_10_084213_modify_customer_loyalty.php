<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyCustomerLoyalty extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_loyalty', function (Blueprint $table) {
            $table->boolean('reminder_send')->default(0)->after('loyalty_remain');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('customer_loyalty', function (Blueprint $table) {
            $table->dropColumn('reminder_send');
        });
    }
}
