<?php

namespace Pukudada\Loyalty;

use Illuminate\Database\Eloquent\Model;

class CustomerLoyalty extends Model
{
    protected $table = 'customer_loyalty';

    protected $guarded = [];
}
