@extends('layouts.app')

@section('content')
	<div class="ui main container">
		<div class="heading-section">
			<h1 class="page-heading">Loyalty Setting</h1>
		</div>
		@if(session('success'))
			<div class="ui icon message complete">
				<i class="checkmark icon"></i>
				<div class="content">
					<div class="header">
						{{session('success')}}
					</div>
				</div>
			</div>
		@endif
		
		<div class="main-container">
			<form class="ui form contact-options-website" action="{{route('admin.loyalty-setting.update')}}" method="post">
				{{csrf_field()}}
				<table class="ui celled striped table table-forms">
					<thead>
					<tr>
						<td colspan="4"><h4>Loyalty Settings</h4></td>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td class="label-info" colspan="4">This conversion is for the loyalty point to be provided on the expenses the customer does.</td>
					</tr>
					<tr style="color: #ff2b32">
						<td class="collapsing label-section">Expense Amount ($)</td>
						<td class="input-fields-column">
							<input autofocus class="input-fields" type="number"  value="{{$loyaltysetting->expense_amt}}" name="expense_amt" placeholder="value in $"/>
						</td>
						<td class="collapsing label-section">Loyalty Conversion</td>
						<td class="input-fields-column">
							<input class="input-fields" type="number" name="loyalty_conversion"  value="@isset($loyaltysetting){{$loyaltysetting->loyalty_conversion}}@endisset" placeholder="loyalty conversion point"/></td>
					</tr>
					
					<tr>
						
						<td class="label-info" colspan="4">This conversion is for the monetary value of the loyalty point that is done on redeem .</td>
					</tr>
					
					<tr style="color: #ff2b32">
						<td class="collapsing label-section">Points</td>
						<td class="input-fields-column">
							<input class="input-fields" type="number" value="@isset($loyaltysetting){{$loyaltysetting->points}}@endisset"  name="points" placeholder="loyalty points"/></td>
						<td class="collapsing label-section">Monetary Conversion ($)</td>
						<td class="input-fields-column">
							<input class="input-fields" type="number" value="@isset($loyaltysetting){{$loyaltysetting->monetary_conversion}}@endisset"  name="monetary_conversion" placeholder="monetary value of loyalty points"/></td>
					</tr>
					
					<tr>
						<td class="label-info" colspan="2">The minimum loyalty point the customer should have to redeem for the checkout.</td>
					</tr>
					<tr>
						<td class="collapsing label-section">Redeem Threshold</td>
						<td class="input-fields-column">
							<input class="input-fields" type="text" value="@isset($loyaltysetting){{$loyaltysetting->redeem_threshold}}@endisset"  name="redeem_threshold" placeholder="Redeem point minimum of customer"/></td>
						
					</tr>
					
					</tbody>
					<tfoot>
					<tr>
						<td class="collapsing"></td>
						<td>
							<button  type="submit" class="ui primary button submit wide-button">Save</button>
						</td>
						<td></td>
					</tr>
					</tfoot>
				</table>
			</form>
		</div>
		
	</div>
	
@endsection
