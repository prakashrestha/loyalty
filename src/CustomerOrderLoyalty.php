<?php

namespace Pukudada\Loyalty;

use Illuminate\Database\Eloquent\Model;

class CustomerOrderLoyalty extends Model
{
    protected $table = 'customer_order_loyalty';

    protected $guarded = [];
}
