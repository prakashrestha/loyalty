<?php

namespace Pukudada\Loyalty;

/**
 * This file is part of Entrust,
 * a role & permission management solution for Laravel.
 *
 * @license MIT
 * @package Zizaco\Entrust
 */

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;

class MigrationCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'loyalty:migration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a migration following the Entrust specifications.';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $this->handle();
    }

    /**
     * Execute the console command for Laravel 5.5+.
     *
     * @return void
     */
    public function handle()
    {
        $userTable          = Config::get('loyalty_config.users_table');
        $orderTable       = Config::get('loyalty_config.order_table');
        $loyaltyTable    = Config::get('loyalty_config.loyalty_table');
        $customerLoyaltyTable    = Config::get('loyalty_config.customer_loyalty_table');
        $userModelName = Config::get('auth.providers.users.model');
        $userModel = new $userModelName();
        $usersTable = $userModel->getTable();
        $userKeyName = $userModel->getKeyName();

        $migrationFile = database_path("/migrations")."/2020_10_08_001928_create_customer_order_loyalties_table.php";

        $data = compact('userTable', 'orderTable', 'loyaltyTable', 'usersTable', 'userKeyName','customerLoyaltyTable');

        $output = $this->laravel->view->make('loyalty::generators.migration')->with($data)->render();

        if (!file_exists($migrationFile) && $fs = fopen($migrationFile, 'x')) {
            fwrite($fs, $output);
            fclose($fs);
            return true;
        }

        return false;

    }
}
