<?php
return [

    'loyalty_Model' => 'App\CustomerOrderLoyalty',

    'user_Model' => 'App\Restaurant\Models\ro_user',


    'users_table' => 'ro_users',
    'loyalty_table' => 'loyalty',
    'customer_loyalty_table' => 'customer_loyalty',
    'order_table' => 'ro_orders',


    'user_foreign_key' => 'user_id',

    'order_foreign_key' => 'order_id',
];
